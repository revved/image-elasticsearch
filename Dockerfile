FROM docker.elastic.co/elasticsearch/elasticsearch:7.1.1
ENV discovery.type single-node
RUN bin/elasticsearch-plugin install analysis-phonetic
